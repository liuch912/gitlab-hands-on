##############################

## Variables for TecentCloud TKE Cluster

#############################

# Name for labelling
variable "name" {
  default = "service-a"
}
# Region
variable "region" {
  default = "ap-beijing"
}
# Version of Kubernetes
variable "k8s_ver" {
  default = "1.18.4"
}

# IP range for Pods
variable "pod_ip_seg" {
  default = "172.16"
}

# IP range of VPC
variable "vpc_ip_seg" {
  default = "10.0"
}

# Instance Type
variable "default_instance_type" {
  default = "S5se.2XLARGE32"
}

# Root Password for Nodes
variable "node_password" {
  default = "P@ssw@rd2022"
}
