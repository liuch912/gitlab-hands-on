terraform {
  required_providers {
    tencentcloud = {
      source = "tencentcloudstack/tencentcloud"
    }
  }
}

# Configure the TencentCloud Provider
provider "tencentcloud" {
  region     = var.region
}

# Create security group
resource "tencentcloud_security_group" "sg01" {
  name        = "${var.name}-sg"
  description = "security group created by terraform"
}

# Create security group rules
resource "tencentcloud_security_group_lite_rule" "sg01-rule" {
  security_group_id = tencentcloud_security_group.sg01.id

  ingress = [
    "ACCEPT#0.0.0.0/0#ALL#ICMP",
    "ACCEPT#0.0.0.0/0#22#TCP",
    "ACCEPT#0.0.0.0/0#30000-32768#TCP",
    "ACCEPT#0.0.0.0/0#30000-32768#UDP",
  ]

  egress = [
    "ACCEPT#0.0.0.0/0#ALL#ALL",
  ]
}

# Get available zones from this region
data "tencentcloud_availability_zones" "all_zones" {
}

# Create VPC
resource "tencentcloud_vpc" "vpc01" {
  name         = "${var.name}-01"
  cidr_block   = "${var.vpc_ip_seg}.0.0/16"
  is_multicast = false

  tags = {
    "project" = var.name
  }
}

# Create Subnet
resource "tencentcloud_subnet" "subset01" {
  count             = length(data.tencentcloud_availability_zones.all_zones.zones)
  name              = "${var.name}-subset-${count.index}"
  vpc_id            = tencentcloud_vpc.vpc01.id
  availability_zone = data.tencentcloud_availability_zones.all_zones.zones[count.index].name
  cidr_block        = "${var.vpc_ip_seg}.${count.index}.0/24"
  is_multicast      = false
  tags = {
    "project" = var.name
  }
}

# Create TKE Cluster
resource "tencentcloud_kubernetes_cluster" "tke_managed" {
  vpc_id                                     = tencentcloud_vpc.vpc01.id
  cluster_version                            = var.k8s_ver
  cluster_cidr                               = "${var.pod_ip_seg}.0.0/16"
  cluster_max_pod_num                        = 64
  cluster_name                               = "${var.name}-tke-01"
  cluster_desc                               = "created by terraform"
  cluster_max_service_num                    = 2048
  cluster_internet                           = true
  managed_cluster_internet_security_policies = ["0.0.0.0/0"]
  cluster_deploy_type                        = "MANAGED_CLUSTER"
  cluster_os                                 = "tlinux2.4x86_64"
  container_runtime                          = "containerd"
  deletion_protection                        = false

  worker_config {
    instance_name              = "${var.name}-node"
    availability_zone          = data.tencentcloud_availability_zones.all_zones.zones[0].name
    instance_type              = var.default_instance_type
    system_disk_type           = "CLOUD_SSD"
    system_disk_size           = 50
    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 1
    public_ip_assigned         = true
    subnet_id                  = tencentcloud_subnet.subset01[0].id
    security_group_ids         = [tencentcloud_security_group.sg01.id]

    enhanced_security_service = false
    enhanced_monitor_service  = false
    password                  = var.node_password
  }


  labels = {
    "project" = var.name
  }
}

# Output KubeConfig file
output "KUBECONFIG" {
  description = "Below is the kubeconfig，please keep it saved"
  value       = tencentcloud_kubernetes_cluster.tke_managed.kube_config
}