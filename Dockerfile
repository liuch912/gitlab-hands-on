FROM alpine:3.15.0

MAINTAINER Chang Liu<changliu1991@hotmail.com>

LABEL name="gitlab-hands-on" \
      description="build service-a manually for gitlab hands-on quiz" \
      owner="changliu1991@hotmail.com"

WORKDIR /
COPY ./service-a /service-a
COPY ./config.yaml /config.yaml

EXPOSE 8888
CMD [ "./service-a", "--file", "config.yaml" ]