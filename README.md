# gitlab-hands-on

This repo is made for a delivery quiz.

1. `Dockerfile` for building `service-a`
2. `Terraform` for creating TKE cluster
3. `Helm` charts for deploying `service-a` into a specific TKE cluster

## How to build binary
`GOARCH=amd64` is necessary, if your local environment is x86.
```
env GOOS=linux GOARCH=amd64 go build
```

## How to build docker images

```
docker build . --file Dockerfile --tag liuch912/gitlab-hands-on:v0.1
# Push images to public registry
docker push liuch912/gitlab-hands-on:v0.1
```

## How to create TKE cluster by using Terraform

The `TENCENTCLOUD_SECRET_ID` and `TENCENTCLOUD_SECRET_KEY` need to be set before the execution of terraform command line.

```
# Replace the xxxx with real credentials, then add these into ~/.bash_profile
export TENCENTCLOUD_SECRET_ID=xxxx
export TENCENTCLOUD_SECRET_KEY=xxxx

cd terraform
terraform init
terraform apply

# Export kubeconfig file
terraform output -raw KUBECONFIG > kubeconfig.yaml
```

## How to deploy service-a by using helm charts

```
helm --kubeconfig /Path/of/Your/jihu-handson-tke-kubeconfig.liuchang.yaml -nservice-liuchang  install service-a manifests/charts/service-a -f service-a/values-tke.yaml
```

## Uninstall service-a

```
helm --kubeconfig /Path/of/Your/jihu-handson-tke-kubeconfig.liuchang.yaml -nservice-liuchang  del service-a 
cd terraform
terraform destory
```
